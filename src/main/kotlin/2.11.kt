/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Calculadora
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter the first number:")
    val num1 = scanner.nextInt()
    println("Enter the second number:")
    val num2 = scanner.nextInt()
    println("Enter the type of operation:")
    val operation = scanner.next()
    when(operation){
        "+" -> println(num1 + num2)
        "-" -> println(num1 - num2)
        "*" -> println(num1 * num2)
        "/" -> println(num1 / num2)
        "%" -> println(num1 % num2)
    }
}