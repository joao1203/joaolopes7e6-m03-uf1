/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/28
* TITLE: Distància d'Hamming
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var word1 = scanner.next()
    val word2 = scanner.next()
    var diff = 0
    if(word1.length == word2.length){
        for(i in 0..word1.lastIndex)
            if(word1[i] != word2[i])
                diff++
    }
    else
        println("Entrada no valida!")
    println(diff)
}