/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Calcula l'àrea
*/
import java.util.*

fun main() {
    println("All of the following measures must be inserted in meters.")
    val scanner1 = Scanner(System.`in`)
    println("Insert the width of the room:")
    val width = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Insert the length of the room:")
    val length = scanner2.nextInt()
    print(width * length)
}