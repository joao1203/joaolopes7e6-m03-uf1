/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Rombe d'*
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    var j = 0

    for (i in 1..num) {
        for (space in 1..num - i) {
            print("  ")
        }
        while (j != 2 * i - 1) {
            print("* ")
            ++j
        }
        println()
        j = 0
    }
    for (i in num - 1 downTo 1) {
        for (space in 1..num - i) {
            print("  ")
        }
        while (j != 2 * i - 1) {
            print("* ")
            ++j
        }
        println()
        j = 0
    }
}