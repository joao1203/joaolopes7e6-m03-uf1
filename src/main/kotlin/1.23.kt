/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: És una letra?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert a character:")
    val letter = scanner.next().single()
    val ABC = (letter >= 'A' && letter <= 'Z') || (letter >= 'a' && letter <= 'z')
    println(ABC)
}