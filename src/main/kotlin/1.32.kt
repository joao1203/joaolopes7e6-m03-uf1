/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Calcula el capital
*/
import java.lang.StrictMath.pow
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the capital:")
    val capital = scanner.nextDouble()
    println("Insert how many years have passed")
    val years = scanner.nextDouble()
    println("Insert the amount of interest:")
    val interest = scanner.nextDouble()
    val finalCapital = capital + (years * (capital * (interest * pow(10.0, -2.0))))
    println(finalCapital)
}