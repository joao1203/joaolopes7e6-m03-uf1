/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Té edat per treballar
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the age of the person:")
    val ageWork = scanner.nextInt()
    if ((16 <= ageWork) && (ageWork <= 65))
        println("Té edat per treballar.")
    else
        println("No té edat per treballar.")
}