/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Dobla l'enter
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Insert a number: ")
    val userInputValue = scanner.nextInt()
    println(userInputValue * 2)
}