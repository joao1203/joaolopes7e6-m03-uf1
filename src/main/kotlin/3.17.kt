/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/21
* TITLE: Factorial!
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    var factorial = 1
    for(i in 1..num){
        factorial *= i
    }
    println(factorial)
}