/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Igual a l'últim
*/

fun main(args: Array<String>) {
    var repeat = 0
    for (i in args){
        if(i == args.last())
            repeat++
    }
    println(repeat-1)
}