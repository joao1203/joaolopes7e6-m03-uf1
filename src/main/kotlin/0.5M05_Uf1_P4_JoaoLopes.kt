/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/13
DESCRIPTION: exercise 9 of the fourth practice of M05
 */
fun main(){
    val m= 23
    val n = 12
    var resultat = 0
    resultat = m + n
    println(resultat)
    resultat += 6
    resultat *= n
    println(resultat)
    resultat *= m
    resultat += 5
    println(resultat)
}