/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Nombres decimals iguals
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val firstNum = scanner.nextFloat()
    println("Insert the second number:")
    val secNum = scanner.nextFloat()
    println(firstNum == secNum)
}