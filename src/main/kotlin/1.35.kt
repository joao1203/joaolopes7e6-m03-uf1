/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Creador de targetes de treball
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the workers first name:")
    val firstName = scanner.nextLine()
    println("Insert the workers last name/s:")
    val lastName = scanner.nextLine()
    println("Insert the workers office")
    val office = scanner.nextInt()
    println("Employee: $firstName $lastName - Office: $office")
}