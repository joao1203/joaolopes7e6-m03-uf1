/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: És primer?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    var prime = false
    for (i in 2..num /2) {
        if (num % i == 0) {
            prime = true
            break
        }
    }
    if (!prime)
        println("It's a prime number.")
    else
        println("It's not a prime number.")
}