/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/13
DESCRIPTION: exercise 11 of the fourth practice of M05
 */
fun main(){
    var numA = 5
    var numB = -5
    println("$numA is the first variable")
    println("$numB is the second variable")
    numA = numB.also { numB = numA }

    println("$numA is now the first variable")
    println("$numB is now the second valuable")
}