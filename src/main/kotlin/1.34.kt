/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Hola Usuari
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert your name:")
    val name = scanner.next()
    println("Good Morning ${name}!")
}