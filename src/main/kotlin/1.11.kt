/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Calculadora de volum d'aire
*/
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the width of the room:")
    val widthRoom = scanner.nextDouble()
    println("Insert the length of the room")
    val lengthRoom = scanner.nextDouble()
    println("Insert the height of the room:")
    val heightRoom = scanner.nextDouble()
    val airCapacity = widthRoom * lengthRoom * heightRoom
    val roundAirCapacity = BigDecimal(airCapacity).setScale(2, RoundingMode.HALF_EVEN)
    println(roundAirCapacity)
}