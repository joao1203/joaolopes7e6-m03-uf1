/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Transforma l'enter
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert an integer number:")
    val oldInt = scanner.nextInt()
    println(oldInt.toFloat())
}