/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Afegeix un segon(2)
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter the hours:")
    var hours = scanner.nextInt()
    println("Enter the minutes:")
    var minutes = scanner.nextInt()
    println("Enter the seconds:")
    var seconds = scanner.nextInt()
    if (seconds +1 >= 60){
        seconds = 0
        minutes += 1
    }
    if (minutes >= 60){
        minutes = 0
        hours += 1
    }
    if (hours == 24) hours = 0

    val secondsToPrint = if (seconds < 10) "0$seconds" else seconds
    val minutesToPrint = if (minutes < 10) "0$minutes" else minutes
    val hoursToPrint = if (hours < 10) "0$hours" else hours

    println("$hoursToPrint:$minutesToPrint:$secondsToPrint")
}