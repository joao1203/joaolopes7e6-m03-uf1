/*
Programa que rep un nombre n i a continuacio una matriu de n·n nombres enters
 */

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val MatrixNumber: MutableList<MutableList<Int>> = mutableListOf()

    for (i in 0 until n){
        val mutableListOfNumber = mutableListOf<Int>()
        for (j in 0 until n){
            mutableListOfNumber.add(scanner.nextInt())
        }
        MatrixNumber.add(mutableListOfNumber)
    }
    print(MatrixNumber)
}