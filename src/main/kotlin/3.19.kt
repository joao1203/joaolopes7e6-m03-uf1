/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/21
* TITLE: Multiplicador de inters
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    var zero = 0
    var result:Long = 1
    for (i in 1..num){
        for(j in 1..i){
            if (i%j==0) zero++
        }
        if (zero == 2){
            result *= i
        }
        zero = 0
    }
    println(result)
}