/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/27
* TITLE: Ordena l’array (up)
*/

fun main(args: Array<String>){
    var order = 0
    var num = 0
    var num1 = 1
    do{
        order = 0
        num= 0
        num1 = 1
        for (i in 0 until args.lastIndex){
            if(args[num]>args[num1]){
                val fixed = args[num1]
                args[num1] = args[num]
                args[num] = fixed
                order++
                num++
                num1++
            }
            else{
                num++
                num1++
            }
        }
    }while(order != 0)
    for(i in 0..args.lastIndex){
        print("${args[i]} ")
    }
}