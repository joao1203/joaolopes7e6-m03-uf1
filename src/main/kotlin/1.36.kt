/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Construeix la història
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first part of the story:")
    val story1 = scanner.nextLine()
    println("Insert the second part of the story:")
    val story2 = scanner.nextLine()
    println("Insert the last part of the story:")
    val story3 = scanner.nextLine()
    println("$story1 $story2 $story3")
}