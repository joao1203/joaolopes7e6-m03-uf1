/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Conversor d’unitats
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a weight:")
    val weightVal = scanner.nextDouble()
    println("Enter the amount (G, KG, TON):")
    val weightAmo = scanner.next()
    if (weightAmo == "G"){
        println("${weightVal / 1000}KG")
        println("${weightVal / 1000000}TN")}
    else if (weightAmo == "KG"){
        println("${weightVal * 1000}G")
        println("${weightVal / 1000}TN")}
    else{
        println("${weightVal * 1000}KG")
        println("${weightVal * 1000000}G")}
}