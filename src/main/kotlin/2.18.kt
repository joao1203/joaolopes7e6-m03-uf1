/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Valor absolut
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val numAbs = scanner.nextInt()
    if (numAbs < 0)
        println(numAbs * -1)
    else
        println(numAbs)
}