/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: De metre a peu
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the measure in meters:")
    val meters = scanner.nextDouble()
    val feet = meters * 3.281
    println(feet)
}