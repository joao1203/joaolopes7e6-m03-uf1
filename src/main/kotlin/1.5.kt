/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Operació boja
*/
import java.util.*

fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Insert the first number:")
    val crazy1 = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Insert the second number:")
    val crazy2 = scanner2.nextInt()
    val scanner3 = Scanner(System.`in`)
    println("Insert the third number:")
    val crazy3 = scanner3.nextInt()
    val scanner4 = Scanner(System.`in`)
    println("Insert the fourth number:")
    val crazy4 = scanner4.nextInt()
    println((crazy1 * crazy2) * (crazy3 % crazy4))
}