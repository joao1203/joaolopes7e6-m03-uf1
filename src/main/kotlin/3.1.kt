/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Pinta X números
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val times = scanner.nextInt()
    for (i in 1..times){
        println(i)
    }
}