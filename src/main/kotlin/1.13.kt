/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Quina temperatura fa?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the temperature before the increase in it:")
    val temp1 = scanner.nextDouble()
    println("Insert the amount the temperature increased by:")
    val increase = scanner.nextDouble()
    val tempFinal = (temp1 + increase)
    println("The current temperature is ${tempFinal}º")
}
