/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: És un bitllet vàlid
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the value of an euro bill:")
    val euroBill = scanner.nextInt()
    println((euroBill==5) || (euroBill==10) ||(euroBill==20) ||(euroBill==50) ||(euroBill==100) ||(euroBill==200) ||(euroBill==500))
}