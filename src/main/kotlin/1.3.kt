/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Suma de dos nombres enters
*/
import java.util.*

fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Insert the first number:")
    val input1 = scanner1.nextInt()
    val scanner2 = Scanner(System.`in`)
    println("Insert the second number:")
    val input2 = scanner2.nextInt()
    print(input1 + input2)
}