/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/28
* TITLE: Són iguals?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val word1 = scanner.next()
    val word2 = scanner.next()
    if(word1 == word2)
        println("Son iguals!")
    else
        println("No son iguals!")
}