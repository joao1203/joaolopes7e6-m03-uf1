/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/02
* TITLE: Valors repetits
*/

fun main(args: Array<String>) {
    for(i in 0..args.lastIndex){
        for (j in i + 1..args.lastIndex){
            if (args[j] == args[i]) print("${args[i]} ")
        }
    }
}