/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/28
* TITLE: Purga de caracters
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var word1 = scanner.next()
    var char = ""
    do{
        char = scanner.next()
        word1 = word1.replace(char,"")
    }while(char != "0")
    println(word1)
}