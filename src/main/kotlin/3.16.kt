/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/21
* TITLE: Coordenades en moviment
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var userInput : Char
    var posX = 0
    var posY = 0
    do{
        userInput = scanner.next().single()
        when(userInput){
            'e' -> posX++
            'o' -> posX--
            's' -> posY++
            'n' -> posY--
        }
    }while(userInput != 'z')
    println("(${posX},${posY})")
}