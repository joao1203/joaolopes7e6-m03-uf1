/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: És divisible
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val num1 = scanner.nextInt()
    println("Insert the second number:")
    val num2 = scanner.nextInt()
    val divisible = num2 % num1 == 0
    println(divisible)
}