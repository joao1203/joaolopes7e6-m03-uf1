/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Imprimeix el rang
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num1 = scanner.nextInt()
    val num2 = scanner.nextInt()
    for (i in num1 until num2){
        print("${i},")
    }
    print(num2)
}