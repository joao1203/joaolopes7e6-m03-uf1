/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Ajuda per la màquina de viatge en el temps
*/
import java.time.LocalDate

fun main(){
    val date1 = LocalDate.now()
    println("Today is $date1")
}