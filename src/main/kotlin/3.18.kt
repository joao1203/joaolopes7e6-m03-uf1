/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/21
* TITLE: Fibonacci
*/
import java.util.*


fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    var untilThere = scanner.nextInt()
    var num1 = 0
    var num2 = 1
    for (i in 1..untilThere) {
        print("$num1 ")
        val sum = num1 + num2
        num1 = num2
        num2 = sum
    }
}