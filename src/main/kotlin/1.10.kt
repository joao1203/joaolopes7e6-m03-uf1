/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Quina és la mida de la meva pizza?
*/
import java.lang.StrictMath.pow
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.math.PI

fun main() {
    val scanner = Scanner(System.`in`)
    println("Insert the diameter of your pizza:")
    val diameterPizza = scanner.nextDouble()
    val areaPizza = PI * (pow((diameterPizza / 2),2.0))
    val roundPizza = BigDecimal(areaPizza).setScale(2,RoundingMode.HALF_EVEN)
    println(roundPizza)
}