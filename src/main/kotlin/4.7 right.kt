// rebo un array per paràmetre i he de crear
//un array diferent amb el primer invertit

fun main(args: Array<String>){
    val invertedArray = Array(args.size){"0"}
    for (i in 0..args.lastIndex){
        invertedArray[i] = args[args.lastIndex-i]
    }
    for(j in invertedArray){
        print("$j ")
    }
}