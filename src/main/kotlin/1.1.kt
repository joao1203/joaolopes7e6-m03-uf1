/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Hello World!
*/
fun main() {
    print("Hello World!")
}