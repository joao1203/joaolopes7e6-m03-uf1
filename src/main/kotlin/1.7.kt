/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Número seguent
*/
import java.util.*

fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Insert a number:")
    val followingnumber = scanner1.nextInt()
    print("After comes ")
    print(followingnumber + 1)
}