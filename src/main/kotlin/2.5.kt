/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: En rang
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val num1 = scanner.nextFloat()
    println("Insert the second number:")
    val num2 = scanner.nextFloat()
    println("Insert the third number:")
    val num3 = scanner.nextFloat()
    println("Insert the fourth number:")
    val num4 = scanner.nextFloat()
    println("Insert the fifth number:")
    val num5 = scanner.nextFloat()
    val rang1 = num1..num2
    val rang2 = num3..num4
    println((num5 in rang1) && (num5 in rang2))
}