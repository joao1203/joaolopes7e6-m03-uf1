/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/14
TITLE: Any de traspàs
 */
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter a year:")
    val yearValue = scanner.nextInt()
    if (yearValue % 4 == 0)
        println("It is a leap year!")
    else
        println("It is not a leap year!")
}