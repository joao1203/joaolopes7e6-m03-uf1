/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Extrems
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a list of numbers:")
    var nums = scanner.nextInt()
    var minNum = nums
    var maxNum = nums
    while (nums != 0){
        nums = scanner.nextInt()
        if (nums < minNum && nums != 0) {
            minNum = nums
        }
        else if (nums > maxNum && nums != 0){
            maxNum = nums
        }
    }
    println("$minNum $maxNum")
}