/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Endevina el número
*/
import java.util.*
import java.math.*

fun main() {
    val scanner = Scanner(System.`in`)
    val secret = (0..10).random()
    val num = 0

    while (num != secret){
        val num = scanner.nextInt()
        if (num > secret) println("Too High!")
        else if (num < secret) println("Too Low!")
        else println("That is the secret number!")
    }
}