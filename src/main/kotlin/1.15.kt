/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Afegeix un segon
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert an amount of seconds:")
    val sec = scanner.nextInt()
    val sec1 = ((sec + 1)%60)
    println(sec1)
}