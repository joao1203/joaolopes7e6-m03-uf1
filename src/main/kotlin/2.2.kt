/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Salari
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the amount of hours worked:")
    val hours = scanner.nextInt()
    if (hours <= 40)
        println(hours * 40)
    else
        println(((hours -40) * 60)+1600)
}