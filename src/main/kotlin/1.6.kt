/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Pupitres
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Insert the number of students in class 1:")
    val students1 = scanner.nextInt()
    println("Insert the number of students in class 2:")
    val students2 = scanner.nextInt()
    println("Insert the number of students in class 3:")
    val students3 = scanner.nextInt()

    val finalClass1 = ((students1 / 2) + (students1 % 2))
    val finalClass2 = ((students2 / 2) + (students2 % 2))
    val finalClass3 = ((students3 / 2) + (students3 % 2))
    println(finalClass1 + finalClass2 + finalClass3)
}
