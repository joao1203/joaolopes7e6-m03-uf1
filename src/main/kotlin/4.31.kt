/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/18
* TITLE: Comptant a’s
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val phrase = scanner.nextLine()
    var count = 0

    for (i in phrase){
        if (i == '.'){
            break
        }
        if (i == 'a'){
            count++
        }
    }
    println(count)
}