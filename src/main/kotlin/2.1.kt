/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/09
* TITLE: Màxim de 3 nombres enters
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val num1 = scanner.nextFloat()
    println("Insert the second number:")
    val num2 = scanner.nextFloat()
    println("Insert the third number:")
    val num3 = scanner.nextFloat()
    if (num1 >= num2 && num1 >= num3)
        println("$num1 is the largest number.")
    else if (num2 >= num1 && num2 >= num3)
        println("$num2 is the largest number.")
    else
        println("$num3 is the largest number.")
}