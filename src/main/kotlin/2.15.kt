/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/14
TITLE: És vocal o consonant?
 */
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a letter:")
    val letter = scanner.next().single()
    if ((letter == 'a')||(letter == 'e')||(letter == 'i')||(letter == 'o')||(letter == 'u')||(letter == 'A')||(letter == 'E')||(letter == 'I')||(letter == 'O')||(letter == 'U'))
        println("It is a vowel!")
    else
        println("It is a consonant!")
}