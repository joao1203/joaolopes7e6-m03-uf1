/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/28
* TITLE: Hola i Adeu
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val word = scanner.next()

    if("hola" in word){
        println("hola")
    }
    else if("adeu" in word){
        println("adeu")
    }
    else{
        println("No se ha detectado ni 'Hola' ni 'Adeu'")
    }
}