/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Quina nota he tret?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter your grade:")
    val grade = scanner.nextDouble()
    when (grade) {
        in 9.0..10.0 -> println("Excellent!")
        in 7.0..8.9 -> println("Notable!")
        in 6.0..6.9 -> println("Ok!")
        in 5.0..5.9 -> println("Sufficient!")
        in 0.0..4.9 -> println("Insufficient!")
        else -> println("Invalid grade!")
    }
}