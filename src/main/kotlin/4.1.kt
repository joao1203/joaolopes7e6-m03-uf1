/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Suma els valors
*/
import java.util.*

fun main(args: Array<String>){
    var result = 0
    for (i in args.iterator()){
        result += i.toInt()
    }
    println(result)
}