/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Quant de temps?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Inset an amount of seconds:")
    val seconds1 = scanner.nextInt()
    val hours = seconds1 / 3600
    val minutes = (seconds1 % 3600) / 60
    val seconds = seconds1 % 60
    println("$hours hours $minutes minutes $seconds seconds")
}