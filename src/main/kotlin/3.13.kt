/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Logaritme natural de 2
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    var num = scanner.nextInt()
    var res = 0.0
    for(i in 1..num){
        if (i % 2 == 0 )
            res -= 1.0/i
        else
            res += 1.0/i
    }
    println(res)
}