/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Eleva'l
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter the base number:")
    var num1 = scanner.nextLong()
    println("Enter the number that the first one will be elevated by:")
    val num2 = scanner.nextLong()
    var result = 1L
    for (i in 1..num2) {
        result *= num1
    }
    println(result)
}