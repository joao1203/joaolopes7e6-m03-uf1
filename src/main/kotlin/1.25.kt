/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Fes-me majúscula
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert a letter:")
    val letter = scanner.next().single()
    println( letter - 32)
}