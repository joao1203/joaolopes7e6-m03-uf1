/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/14
TITLE: És vocal o consonant?
 */
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a month by its respective number:")
    when (scanner.nextInt()){
        1 -> println(31)
        2 -> println(28)
        3 -> println(31)
        4 -> println(30)
        5 -> println(31)
        6 -> println(30)
        7 -> println(31)
        8 -> println(31)
        9 -> println(30)
        10 -> println(31)
        11 -> println(30)
        12 -> println(31)
    }
}