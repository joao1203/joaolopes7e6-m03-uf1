/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Calcula la mitjana
*/
import java.util.*

fun main(args: Array<String>){
    var result = 0.0
    var totalNum = 0.0
    for (i in args.iterator()){
        result += i.toDouble()
        ++totalNum
    }
    println(result / totalNum)
}