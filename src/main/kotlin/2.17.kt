/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Puges o baixes?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter the first number:")
    val num1 = scanner.nextInt()
    println("Enter the second number:")
    val num2 = scanner.nextInt()
    println("Enter the third number:")
    val num3 = scanner.nextInt()
    if (num1 < num2 && num1 < num3 && num2 < num3)
        println("Ascending!")
    else if (num1 > num2 && num1 > num3 && num2 > num3)
        println("Descending!")
    else
        println("Neither!")
}