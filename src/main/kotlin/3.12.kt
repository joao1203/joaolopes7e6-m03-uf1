/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Divisible per 3 i per 5
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    var num = scanner.nextInt()
    for(i in 1..num){
        if(i % 3 == 0 && i % 5 == 0)
            println("$i is divisible by 3 and 5")
        else if(i % 3 == 0)
            println("$i is divisible by 3")
        else if(i % 5 == 0)
            println("$i is divisible by 5")
    }
}