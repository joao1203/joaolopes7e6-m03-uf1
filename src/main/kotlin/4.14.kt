/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/28
* TITLE: Quants sumen...?
*/
import java.util.*

fun main(args: Array<String>){
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val sum = scanner.nextInt()
    for(i in 0..args.lastIndex){
        for (j in 0 until args.last().toInt()){
            if (args[i].toInt() + args[j].toInt() == sum)
                println("${args[i]} ${args[j]}")
        }
    }
}