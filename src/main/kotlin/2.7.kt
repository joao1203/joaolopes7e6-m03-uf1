/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Parell o senar?
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    print("Insert a number:")
    val num = scanner.nextInt()
    if (num % 2 == 0)
        println("$num is an even number")
    else
        println("$num is an odd number")
}