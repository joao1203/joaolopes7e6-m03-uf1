/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/28
* TITLE: Palíndrom
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    val word = scanner.next()
    var reverse = ""

    for(i in word.length - 1 downTo 0){
        reverse += word[i]
    }
    if(reverse == word){
        println(reverse)
        println("It is palindrome")
    }
    else{
        println(reverse)
        println("It is not a palindrome")
    }
}