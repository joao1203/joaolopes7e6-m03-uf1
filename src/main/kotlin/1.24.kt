/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: És un nombre?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert a character")
    val num = scanner.next().single()
    val IsNum = (num >= '0' && num <= '9')
    println(IsNum)
}