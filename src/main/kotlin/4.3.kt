/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Calcula la lletra del dni
*/
import java.util.*

fun main(args: Array<String>){
    val scanner = Scanner(System.`in`)
    println("Enter the digits of the DNI:")
    val numDNI = scanner.nextInt()
    var dni = arrayOf("T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E")
    println("${numDNI}${dni[numDNI%23]}")
}