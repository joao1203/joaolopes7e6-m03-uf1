/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Taula de multiplicar
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num1 = scanner.nextInt()
    for(i in 1..10){
        println("${num1}x${i}= ${num1 * i}")
    }
}