/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: En quina posició?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter the digits of the DNI:")
    val num = scanner.nextInt()
    val numList = arrayOf(1,2,3,4,5,6,7,8,9,10)
    if (num in numList)
        println(numList.indexOf(num))
    else
        println("The number is not contained!")
}