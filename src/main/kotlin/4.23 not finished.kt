/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/28
* TITLE: Inverteix les paraules
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var word = scanner.next()
    while(word != "0"){
        var reverse = ""

        for(i in word.length - 1 downTo 0){
            reverse += word[i]
        }
        println(reverse)
    }
}