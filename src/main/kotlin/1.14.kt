/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Quina temperatura fa?
*/
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert how many people are going to pay the bill equally:")
    val people = scanner.nextInt()
    println("Insert the amount that will be paid:")
    val cost = scanner.nextDouble()
    val costEach = (cost / people)
    val costEachRound = BigDecimal(costEach).setScale(2, RoundingMode.HALF_EVEN)
    println("Each person will have to pay ${costEachRound}€")
}