/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: De Celsius a Fahrenheit
*/
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Insert the temperature in Celsius:")
    val celsius = scanner.nextDouble()
    val farehn = ((celsius * 9/5) + 32)
    val roundFarehn = BigDecimal(farehn).setScale(2, RoundingMode.HALF_EVEN)
    print(roundFarehn)
}