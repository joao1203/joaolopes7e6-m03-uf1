/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Quina pizza és més gran?
*/
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.math.PI

fun main() {
    val scanner = Scanner(System.`in`)
    println("Insert the diameter of the circular pizza:")
    val diameterPizza = scanner.nextDouble()
    val areaPizza = PI * (StrictMath.pow((diameterPizza / 2), 2.0))
    println("Insert the first side of the square pizza:")
    val squarePizza = scanner.nextInt()
    println("Insert the second side of the square pizza:")
    val squarePizza1 = scanner.nextInt()
    val fullPizza = squarePizza * squarePizza1
    if (areaPizza > fullPizza)
        println("Circular Pizza: $areaPizza")
    else
        println("Square Pizza: $fullPizza")
}