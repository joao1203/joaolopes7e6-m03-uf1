/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/27
* TITLE: Busca el que falta
*/

fun main(args: Array<String>){
    val min = args[0].toInt()
    val max = args[args.lastIndex].toInt()
    var missing = 0
    for(i in min until max){
        if(i != args[missing].toInt()){
            println(i)
            break
        }
        missing++
    }
}