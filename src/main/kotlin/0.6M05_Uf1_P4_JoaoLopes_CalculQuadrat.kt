/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/13
DESCRIPTION: exercise 10 of the fourth practice of M05
 */
import java.lang.StrictMath.pow

fun main(){
    val costat = 5
    val perimetre = costat * 4
    println("El perimetre del cuadrat es $perimetre")
    val area = pow(costat.toDouble(), 2.0)
    println("L'area del cuadrat es $area")
}