/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Qui riu últim riu millor
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val Num1 = scanner.nextInt()
    println("Insert the second number:")
    val Num2 = scanner.nextInt()
    println("Insert the third number:")
    val Num3 = scanner.nextInt()
    println("Insert the fourth number:")
    val Num4 = scanner.nextInt()
    println("Insert the fifth number:")
    val Num5 = scanner.nextInt()
    println((Num1 < Num5) && (Num2 < Num5) && (Num3 < Num5) && (Num4 < Num5))
}