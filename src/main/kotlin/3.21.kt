/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Triangle invertit d'*
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    for(i in num downTo 1){
        for (space in 1..num - i) {
            print("  ")
        }
        for (j in 1..i) {
            print("* ")
        }
        println()
    }
}