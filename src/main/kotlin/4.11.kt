/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/27
* TITLE: Quants parells i quants senars?
*/

fun main(args: Array<String>){
    var spare = 0
    var even = 0
    for (arg in args){
        if(arg.toInt()%2==0)
            spare++
        else
            even++
    }
    println("Parells: $spare")
    println("Senars: $even")
}