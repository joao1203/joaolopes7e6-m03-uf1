/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Equacions de segon grau
*/
import java.lang.StrictMath.pow
import java.util.*
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val Num1 = scanner.nextDouble()
    println("Insert the second number:")
    val Num2 = scanner.nextDouble()
    println("Insert the third number:")
    val Num3 = scanner.nextDouble()
    println(((-Num2) - sqrt(pow(Num2,2.0) -(4 * Num1 * Num3))) / (2 * Num1))
    println(((-Num2) + sqrt(pow(Num2,2.0) -(4 * Num1 * Num3))) / (2 * Num1))
}