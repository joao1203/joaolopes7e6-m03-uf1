/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Canvi mínim
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter the amount without the cents:")
    var euroNoCents = scanner.nextInt()
    println("Enter the amount of cents:")
    var euroCents = scanner.nextInt()
    if (euroCents > 99){
        euroCents = 0
        euroNoCents += 1
    }
}