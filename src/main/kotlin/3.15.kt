/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/20
* TITLE: Endevina el número (ara amb intents)
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val secret = (1..10).random()
    var num = 0
    var counter = 0

    do{
        num = scanner.nextInt()
        ++counter
        if(counter == 5){
            println("You lost!")
            break
        }
        if (num == secret) println("That is the secret number!")
        else if (num < secret) println("Too Low!")
        else println("Too High!")
    }while(num != secret)
}