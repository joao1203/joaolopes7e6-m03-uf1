/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Revers de l'enter
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    var num = scanner.nextInt()
    var reversed = 0
    while (num != 0) {
        val digit = num % 10
        reversed = reversed * 10 + digit
        num /= 10
    }
    println(reversed)
}