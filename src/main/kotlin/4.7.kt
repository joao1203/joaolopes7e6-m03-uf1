/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Inverteix l’array
*/


fun main(args: Array<String>) {
    args.reverse();
    println(args.contentToString())
}