/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/06
* TITLE: Calcula el descompte
*/
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

fun main() {
    val scanner1 = Scanner(System.`in`)
    println("Insert the price of the item before the discount:")
    val beforePrice = scanner1.nextDouble()
    println("Insert the price of the item after the discount:")
    val afterPrice = scanner1.nextDouble()
    val discount = (((afterPrice - beforePrice) / beforePrice) * -100)
    val primeDiscount = BigDecimal(discount).setScale(2, RoundingMode.HALF_EVEN)
    println(primeDiscount)
}