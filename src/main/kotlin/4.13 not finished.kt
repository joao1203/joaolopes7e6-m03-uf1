/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/27
* TITLE: Ordena l’array (down)
*/

fun main(args: Array<String>){
    args.sortDescending()
    for(i in args){
        print("$i ")
    }
}