/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: És edat legal
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter your age:")
    val age = scanner.nextInt()
    println(age >= 18)
}