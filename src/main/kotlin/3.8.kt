/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/19
* TITLE: Nombre de dígits
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val original = scanner.nextInt()
    var num = original
    var count = 0
    while (num != 0) {
        num /= 10
        ++count
    }
    println("The number $original has $count digits!")
}