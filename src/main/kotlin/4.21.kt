/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/11/09
* TITLE: Parèntesis
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    var para = scanner.next()
    var count = 0
    for (i in para){
        if (i == '('){
            count++
        }
        else{
            count--
        }
        if (count < 0){
            println("no")
            break
        }
    }
    if (count != 0){
        println("no")
    }
    else{
        println("si")
    }
}