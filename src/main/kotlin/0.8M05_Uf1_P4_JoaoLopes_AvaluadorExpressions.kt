/*
AUTHOR: Joao Lopes Dias
DATE: 2022/10/13
DESCRIPTION: exercise 12 of the fourth practice of M05
 */
fun main(){
    val a = 2
    val b = 3
    val c = 1
    val d = -3
    println(2 - a * b + d)
    println((2-b) * a + c)
    println(b * a - d * a - c)
    println(d / 3 - b)
    println(b / (33-a))
    println(a * 23 - 1 + c)
}