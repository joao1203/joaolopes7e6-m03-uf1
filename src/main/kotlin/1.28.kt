/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/07
* TITLE: Un és 10
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Insert the first number:")
    val Num1 = scanner.nextInt()
    println("Insert the second number:")
    val Num2 = scanner.nextInt()
    println("Insert the third number:")
    val Num3 = scanner.nextInt()
    println("Insert the fourth number:")
    val Num4 = scanner.nextInt()
    println((Num1 == 10) || (Num2 == 10) || (Num3 == 10) || (Num4 ==10))
}
