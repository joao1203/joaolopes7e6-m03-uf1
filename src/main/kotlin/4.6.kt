/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Mínim i màxim
*/

fun main(args: Array<String>) {
    var min = args[0].toInt()
    var max = args[0].toInt()

    for(i in args){
        val j = i.toInt()
        if (j < min) min = j
        if (j > max) max = j
    }
    print("$min $max")
}
