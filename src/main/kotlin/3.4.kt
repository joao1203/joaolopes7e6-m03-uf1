/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Imprimeix el rang(2)
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter the first number for the range:")
    val num1 = scanner.nextInt()
    println("Enter the second number for the range:")
    val num2 = scanner.nextInt()
    if (num1 < num2){
        for (i in num1 until num2){
            print("${i},")
        }
        print(num2)}
    else{
        for (i in num1 downTo num2 +1){
            print("${i},")
        }
        print(num2)
    }
}