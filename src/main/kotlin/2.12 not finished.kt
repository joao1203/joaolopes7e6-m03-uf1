/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/13
* TITLE: Comprova la data
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Enter a day:")
    val day = scanner.nextInt()
    println("Enter a month:")
    val month = scanner.nextInt()
    println("Enter a year:")
    val year = scanner.nextInt()
    when (month){
        1 -> day in 1..31
        2 -> day in 1..28
        3 -> day in 1..31
        4 -> day in 1..30
        5 -> day in 1..31
        6 -> day in 1..30
        7 -> day in 1..31
        8 -> day in 1..31
        9 -> day in 1..30
        10 -> day in 1..31
        11 -> day in 1..30
        12 -> day in 1..31
    }
    if ((month in 1..12) && (year in 0..2022))
        println("The date is correct.")
    else
        println("The date is incorrect.")
}