/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/26
* TITLE: Igual a l'últim
*/

fun main(args: Array<String>) {
    val last = args.last()
    var repeat = 0
    for(i in 0 until args.lastIndex){
        if(args[i] == last)
            repeat++
    }
    print(repeat)
}