/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/21
* TITLE: Triangle de nombres
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    for(i in 1..num){
        for (j in 1..i) {
            print("$j ")
        }
        println()
    }
}