/*
* AUTHOR: Joao Lopes Dias
* DATE: 2022/10/14
* TITLE: Calcula la suma dels N primers
*/
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    println("Enter a number:")
    val num = scanner.nextInt()
    var times = 0
    for (i in 1..num){
        times += i
    }
    println(times)

}